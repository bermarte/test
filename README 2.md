## Virtual-project-Threeejs #
Virtual gallery project. Built with Three.js and JavaScript.   
- in the folder `exercises` we made some small exercises that are suitable for people who just started learning Three.js.
- in the folder `tests` we made more more complex tests to implement different features.
# Table of contents #
​
- [Virtual gallery](#Virtual-project-Threeejs)
- [Table of contents](#table-of-contents)
    - [General](#code-contribution)
	- [Code Contribution](#code-contribution)
		- [Branches](#branches)
	- [Environments](#environments)
		- [Development](#development)
		- [Production](#production)
		- [Deployment](#deployment)
		- [Start & Stop](#start--stop)
	- [Project Context](#project-context)
		- [Details](#details)
		- [Team](#team)
## General ##

Online Virtual Space where brands can showcase their services/products.

## Code Contribution ##

[repository](https://gitlab.com/studiohyperdrive/projects/studio-hyperdrive/studio-hyperdrive_virtual-project_threejs_2021/)
### Branches ###
We follow these naming conventions:
​
* **master**: Development code.
* **feature/***: For developing new features.
* **fix(feature)***: To fix feature.

## Environments ##

### Development ###

The development environment receives automatic builds when code is contributed to the `master`-branch. This environment is expected to break from time to time and thus should be used for **internal testing only**!

to run the project:    

`
npm install
npm run dev
npm run build 
`  

The exercises are in the folder `exercises`.    
For example, to run a specific exercise: 

`
cd exercises
cd move_the_camera
npm install
npm run dev
`

The tests are in the folder `tests`.    
For example, to run a specific test: 

`
cd tests
cd if_statements
npm install
npm run dev
`

**URL**: [virtual gallery](https://studiohyperdrive.gitlab.io/projects/studio-hyperdrive/studio-hyperdrive_virtual-project_threejs_2021/)

### Production ###
The production environment is built manually from the `production`-branch. This environment has to be **stable at all times**. No unvalidated code can be deployed on this environment.
Every time content in the Sanity Studio is changed, this branch will redeploy. 

**URL**: [virtual gallery](https://studiohyperdrive.gitlab.io/projects/studio-hyperdrive/studio-hyperdrive_virtual-project_threejs_2021/)

### Deployment ###

When finishing a new version, you should open a PR to the master branch. When on the master branch everything is automatically deployed to the production environment.
This way content changes will rebuild the website automatically.

### Start & Stop ###

Starting, stopping and restarting this project needs to be done by the DevOps team, contact them on Slack.

## Project Context ##

This project is a Lifelike & Studio Hyperdrive team effort.

### Details ###

* **Client**: Lifelike
* **Start**: 17/10/2022
* **Jira Board**: [Jira Board](https://studiohyperdrive.atlassian.net/jira/software/projects/VSL/boards/266/backlog)
* **Documentation**: [google drive](https://drive.google.com/drive/u/1/folders/1T5ue0-zeM_8bhFJSh7-e4gbDeAmgyhjj)
* **Editor**: [threejs editor](https://threejs.org/editor/)
* **Daily**: [google meet](https://meet.google.com/nvp-jzos-zqv)

### Team ###

* [Ward Peeters](ward.peeters@cronos.be)
    * **Function**: Client
    * **Period**: January 2022 -> ...
* [Justin Cavanas](justin@lifelike.be)
    * **Function**: Project Owner
    * **Period**: January 2022 -> ...
* [Christophe Van Meerbeeck](christophe.vanmeerbeeck@studiohyperdrive.be)
    * **Function**: Project Manager
    * **Period**: January 2022 -> ...
* [Katia Smet](katia.smet@studiohyperdrive.be)
    * **Function**: Tech Lead
    * **Period**: January 2022 -> ...
* [Igor Baeyenst](igor.baeyens@studiohyperdrive.be)
    * **Function**: 3d artist, developer
    * **Period**: January 2022 -> ...
* [Bernardo Martelli](bernardo.martelli@studiohyperdrive.be)
    * **Function**: Developer
    * **Period**: January 2022 -> ...
