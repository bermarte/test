'use strict'
import * as THREE from 'three'

const hemiLight = new THREE.HemisphereLight(0xffffff, 0x444444)
hemiLight.position.set(0, 300, 0)

const dirLight = new THREE.DirectionalLight(0xffffff)
dirLight.position.set(75, 300, -75)

export { hemiLight, dirLight }
