'use strict'
import * as THREE from 'three'

// Plane
const planeGeometry = new THREE.PlaneGeometry(200, 200)
const planeMaterial = new THREE.MeshBasicMaterial({
  color: 0xB8C0BF
})
const plane = new THREE.Mesh(planeGeometry, planeMaterial)
// plane.rotation.x = -Math.PI / 2

export { plane }
