'use strict'
import * as THREE from 'three'
import { cameraPosition } from '../utils/cameraPosition.js'

// Camera
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000)
camera.position.set(cameraPosition.set.x, cameraPosition.set.y, cameraPosition.set.z)
export { camera }
