'use strict'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js'
import { detectPerf } from '../utils/detectPerf.js'
import * as THREE from 'three'

import { scene, skyMat } from '../script.js'
import { hitboxes } from '../const/hitboxes.js'
const perRes = detectPerf()

const dracoLoader = new DRACOLoader()
dracoLoader.setDecoderPath('assets/js/libs/draco/')
const loader = new GLTFLoader()
loader.setDRACOLoader(dracoLoader)
let loadMesh
// show/hide the raycaster
const hideHitboxes = true
let hitboxesGltf

if (perRes === 3) {
  console.log('loading high perf model')
  loadMesh = (filepath) => {
    return new Promise((resolve, reject) => {
      loader.load(filepath, (gltf) => {
        gltf.scene.scale.set(6, 6, 6)
        scene.add(gltf.scene)
        resolve(gltf)
        gltf.scene.traverse((child) => {
          if (child.isMesh && child.name === 'All_Meshes') {
            child.material = skyMat
          } else {
            // hitboxes
            if (hitboxes.includes(child.name)) {
              const basicMat = new THREE.MeshBasicMaterial({ color: 0x0095DD })
              child.material = basicMat
              hitboxesGltf = gltf
              child.material.transparent = true
              if (hideHitboxes) {
                child.material.opacity = 0.0
              }
            }
          }
        })
      }, null, function (error) {
        console.log(`error: ${error}`)
      })
    })
  }
} else {
  console.log('not loading')
  // TODO: SHOW MESSAGE
}

export { loadMesh, hitboxesGltf }
