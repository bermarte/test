'use strict'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
let blenderCamera

const loadBlenderCamera = () => {
  return new Promise((resolve, reject) => {
    const loader = new GLTFLoader()
    const cam = 'assets/models/camera_Closer.glb'
    loader.load(cam, function (gltf) {
      gltf.scene.scale.set(6, 6, 6)
      blenderCamera = gltf
      resolve(gltf)
    })
  })
}
export {
  loadBlenderCamera,
  blenderCamera
}
