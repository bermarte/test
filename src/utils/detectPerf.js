/*
 *  Detect Perf
 *  Detect Performance on init. Can be used to prevent animations from happening on slower devices
 */
import { PERF } from '../const/perf.js'
import { detectIE } from './detectBrowser.js'
let perf
const getPerfs = () => {
  const array = []
  const start = (window.performance || Date).now()
  for (let i = 0; i < 20000; i += 1) {
    const calculation = Math.sin(Math.random()) ** 2
    array.push(calculation)
  }
  const end = (window.performance || Date).now()
  return end - start
}
export const detectPerf = () => {
  const isInternetExplorer = detectIE()
  if (isInternetExplorer) {
    perf = PERF.PERF_BAD
  } else {
    const timing = getPerfs()
    if (timing < 7) {
      perf = PERF.PERF_HIGH
    } else if (timing < 14) {
      perf = PERF.PERF_GOOD
    } else if (timing < 22) {
      perf = PERF.PERF_LOW
    } else {
      perf = PERF.PERF_BAD
    }
  }
  return perf
}
export const getPerf = () => perf
