export const detectIE = () => {
  if (typeof window !== 'undefined' && typeof window.navigator !== 'undefined') {
    const ua = navigator.userAgent
    const isIE = ua.indexOf('MSIE ') > -1 || ua.indexOf('Trident/') > -1
    return isIE
  }
  return true
}
