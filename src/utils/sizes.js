'use strict'
/**
 * Sizes
 */
export const sizes = {
  width: window.innerWidth,
  height: window.innerHeight
}
