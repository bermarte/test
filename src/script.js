'use strict'
import './css/main.css'
import * as THREE from 'three'

import { plane } from './modules/plane.js'
import { sizes } from './utils/sizes.js'
import { handleResize } from './utils/handleResize.js'
import { camera } from './modules/camera.js'
import { blenderCamera, loadBlenderCamera } from './modules/blenderCamera.js'
import { loadMesh, hitboxesGltf } from './modules/loadMesh.js'
import { getJSON } from './utils/getJson.js'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
// import { EXRLoader } from 'three/examples/jsm/loaders/EXRLoader'
// import VirtualScroll from 'virtual-scroll'
import gsap from 'gsap'
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js'
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js'
// import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass.js'
import { UnrealBloomPassAlpha } from './shaders/UnrealBloomPassAlpha.js'
// eslint-disable-next-line no-unused-vars
import { GlitchPass } from 'three/examples/jsm/postprocessing/GlitchPass.js'
import { hitboxes } from './const/hitboxes.js'
// import { TextureFilter } from 'three'
// import * as dat from 'lil-gui'
gsap.registerPlugin(ScrollTrigger)

const COLOR_1 = 0xD7FF00
const COLOR_2 = 0xFFFFFF
const COLOR_3 = 0x00ffbf
const COLOR_4 = 0xbfafdd

// https://stackoverflow.com/questions/50226091/how-three-js-uses-the-gradient-color-for-the-background

// check also https://stackoverflow.com/questions/64560154/applying-color-gradient-to-material-by-extending-three-js-material-class-with-on
// https://stackoverflow.com/questions/20307489/three-js-glsl-convert-pixel-coordinate-to-world-coordinate?noredirect=1&lq=1
const vertexShader = `     
  varying vec2 vUv;
  // uniform float translationX;
  // uniform float translationY;
  // uniform float translationZ;
  // uniform float scaleX;
  // uniform float scaleY;
  // uniform float scaleZ;
  // uniform float rotationX;
  // uniform float rotationY;
  // uniform float rotationZ;
  varying mat4 vPosition;
  void main() {
    vUv = uv;
    vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
    // vPosition =  tPos * rXPos * rZPos * rYPos * sPos;
    gl_Position = projectionMatrix * modelViewPosition; 
    // gl_Position = projectionMatrix * vPosition * modelViewPosition; 
  }
`

const fragmentShader = `
  uniform sampler2D texture1; 
  uniform sampler2D texture2; 
  varying vec2 vUv;
  uniform float translationX;
  uniform float translationY;
  uniform float translationZ;
  uniform float scaleX;
  uniform float scaleY;
  uniform float scaleZ;
  uniform float rotationX;
  uniform float rotationY;
  uniform float rotationZ;
  varying mat4 vPosition;

  void main() {
    // Translate
      mat4 tPos = mat4(vec4(1.0,0.0,0.0,0.0),
                       vec4(0.0,1.0,0.0,0.0),
                       vec4(0.0,0.0,1.0,0.0),
                       vec4(translationX,translationY,translationZ,1.0));
      
      // Rotate
      mat4 rXPos = mat4(vec4(1.0,0.0,0.0,0.0),
                        vec4(0.0,cos(rotationX),-sin(rotationX),0.0),
                        vec4(0.0,sin(rotationX),cos(rotationX),0.0),
                        vec4(0.0,0.0,0.0,1.0));
      
      mat4 rYPos = mat4(vec4(cos(rotationY),0.0,sin(rotationY),0.0),
                        vec4(0.0,1.0,0.0,0.0),
                        vec4(-sin(rotationY),0.0,cos(rotationY),0.0),
                        vec4(0.0,0.0,0.0,1.0));
      
      mat4 rZPos = mat4(vec4(cos(rotationZ),-sin(rotationZ),0.0,0.0),
                        vec4(sin(rotationZ),cos(rotationZ),0.0,0.0),
                        vec4(0.0,0.0,1.0,0.0),
                        vec4(0.0,0.0,0.0,1.0));
                        
      // Scale
      mat4 sPos = mat4(vec4(scaleX,0.0,0.0,0.0),
                       vec4(0.0,scaleY,0.0,0.0),
                       vec4(0.0,0.0,scaleZ,0.0),
                       vec4(0.0,0.0,0.0,1.0));
    // vPosition =  tPos * rXPos * rZPos * rYPos * sPos;
    vec4 col = texture2D(texture1, vUv);
    // vec4 color2 = texture2D(texture2, vUv);
    //gl_FragColor = mix(color1, color2, vUv.y);
    gl_FragColor = col;
  }
`

const loader = new THREE.TextureLoader()
const texture1 = loader.load('assets/textures/tine_Textures_Texture_2.png')
const uniforms = {
  color_1: { value: new THREE.Color(COLOR_1) },
  color_2: { value: new THREE.Color(COLOR_2) },
  color_3: { value: new THREE.Color(COLOR_3) },
  color_4: { value: new THREE.Color(COLOR_4) },
  texture1: { value: texture1 },
  translationX: { value: 1.0 },
  translationY: { value: 1.0 },
  translationZ: { value: 1.0 },
  scaleX: { value: 1.0 },
  scaleY: { value: 1.0 },
  scaleZ: { value: 1.0 },
  rotationX: { value: 1.0 },
  rotationY: { value: 1.0 },
  rotationZ: { value: 1.0 }
}

const skyMat = new THREE.ShaderMaterial({
  uniforms,
  vertexShader,
  fragmentShader
})

// shader UI

/* const gui = new dat.GUI()
const shaderFolder = gui.addFolder('Shader')
shaderFolder.add(uniforms.rotationX, 'value', 1.0, 300.0).name('rot X')
shaderFolder.add(uniforms.rotationY, 'value', 1.0, 300.0).name('rot y')
shaderFolder.add(uniforms.rotationZ, 'value', 1.0, 300.0).name('rot z')
shaderFolder.open() */

// const cameraFolder = gui.addFolder('Camera')
// cameraFolder.add(camera.position, 'z', 0, 10)
// cameraFolder.open()

const phongMaterial = new THREE.MeshPhongMaterial({
  color: 0xFF0000,
  flatShading: true
})
// show/hide console.log
const log = false
// const showRun = true

// Canvas
const canvas = document.querySelector('.c-canvas')

// Scene
const scene = new THREE.Scene()
// scene.background = new THREE.Color(0x82E4053) // #85C1E9
const path = 'assets/models/'
getJSON('data/objects.json').then(
  (data) => {
    data.objects.forEach(item => {
      // if (item.name === 'hitboxes.glb') {
      //   //do something
      // }
      return loadMesh(`${path}${item.file}`)
    })
  }
)

// Load camera
let mixTl
let composer

loadBlenderCamera()
  .then((object) => {
    scene.add(object.scene)
    const model = object.scene
    const mixer = new THREE.AnimationMixer(model)
    const clip1 = object.animations[0]
    const action1 = mixer.clipAction(clip1)
    action1.play()
    mixTl = mixer

    // post effects
    composer = new EffectComposer(renderer)
    composer.addPass(new RenderPass(scene, blenderCamera.cameras[0]))
    // composer.addPass(new GlitchPass())
    composer.addPass(new UnrealBloomPassAlpha({ x: 1024, y: 1024 }, 1.5, 0.8, 0.85))
  })

// get height of client
/* const docHeight = () => {
  const height = window.innerHeight ||
  document.documentElement.clientHeight ||
  document.body.clientHeight
  return height
} */

const percentage = {
  run: 0
}

const tl = gsap.timeline()
  .to(percentage, {
    // last frame
    run: 16.85 // 22.2
  })

ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '0% 0%',
  scrub: 1,
  animation: tl
})
const cases = gsap.timeline()
  .from('.see', { autoAlpha: 0 })
  .to('.see', { autoAlpha: 1 })
const seeAll = gsap.timeline()
  .from('.cases', { autoAlpha: 0 })
  .to('.cases', { autoAlpha: 1 })
const player = gsap.timeline()
  .from('.cases-player', { autoAlpha: 0 })
  .to('.cases-player', { autoAlpha: 1 })
const playerUp = gsap.timeline()
  .from('.cases-player', { top: '79%' })
  .to('.cases-player', { top: '49%', autoAlpha: 0 })
const playerUpSmall = gsap.timeline()
  .from('.cases-player--small', { top: '49%', autoAlpha: 0 })
  .to('.cases-player--small', { top: '20%', autoAlpha: 1 })

const mca = gsap.timeline()
  .from('.cases-mca', { autoAlpha: 0 })
  .to('.cases-mca', { autoAlpha: 1 })
const mcaUp = gsap.timeline()
  .from('.cases-mca', { top: '79%' })
  .to('.cases-mca', { top: '53%', autoAlpha: 0 })
const mcaUpSmall = gsap.timeline()
  .from('.cases-mca--small', { top: '53%', autoAlpha: 0 })
  .to('.cases-mca--small', { top: '28%', autoAlpha: 1 })

const brain = gsap.timeline()
  .from('.cases-brain', { autoAlpha: 0 })
  .to('.cases-brain', { autoAlpha: 1 })
const brainUp = gsap.timeline()
  .from('.cases-brain', { top: '85%' })
  .to('.cases-brain', { top: '57%', autoAlpha: 0 })
const brainUpSmall = gsap.timeline()
  .from('.cases-brain--small', { top: '57%', autoAlpha: 0 })
  .to('.cases-brain--small', { top: '36%', autoAlpha: 1 })

const church = gsap.timeline()
  .from('.cases-church', { autoAlpha: 0 })
  .to('.cases-church', { autoAlpha: 1 })
const churchUp = gsap.timeline()
  .from('.cases-church', { top: '79%' })
  .to('.cases-church', { top: '61%', autoAlpha: 0 })
const churchUpSmall = gsap.timeline()
  .from('.cases-church--small', { top: '61%', autoAlpha: 0 })
  .to('.cases-church--small', { top: '42%', autoAlpha: 1 })
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '8%',
  end: '+=10%',
  scrub: true,
  fastScrollEnd: true,
  animation: cases
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '8%',
  end: '+=10%',
  scrub: true,
  fastScrollEnd: true,
  animation: seeAll
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '8%',
  end: '+=10%',
  scrub: true,
  fastScrollEnd: true,
  animation: player
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '11%',
  end: '+=12%',
  scrub: true,
  fastScrollEnd: true,
  animation: playerUp
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '14%',
  end: '+=15%',
  scrub: true,
  fastScrollEnd: true,
  animation: playerUpSmall
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '18%',
  end: '+=20%',
  scrub: true,
  fastScrollEnd: true,
  animation: mca
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '22%',
  end: '+=23%',
  scrub: true,
  fastScrollEnd: true,
  animation: mcaUp
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '25%',
  end: '+=26%',
  scrub: true,
  fastScrollEnd: true,
  animation: mcaUpSmall
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '37%',
  end: '+=39%',
  scrub: true,
  fastScrollEnd: true,
  animation: brain
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '40%',
  end: '+=41%',
  scrub: true,
  fastScrollEnd: true,
  animation: brainUp
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '43%',
  end: '+=44%',
  scrub: true,
  fastScrollEnd: true,
  animation: brainUpSmall
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '68%',
  end: '+=70%',
  scrub: true,
  fastScrollEnd: true,
  animation: church
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '71%',
  end: '+=72%',
  scrub: true,
  fastScrollEnd: true,
  animation: churchUp
})
ScrollTrigger.create({
  trigger: '.c-scroller',
  start: '74%',
  end: '+=75%',
  scrub: true,
  fastScrollEnd: true,
  animation: churchUpSmall
})

// const keyframes = {
//   player: alert('player'),
//   mca: alert('mca'),
//   brain: alert('brain'),
//   church: alert('church')
// }
// 3.376778 player
// 6.958521 Mac
// 11.227249 brain
// 16.005203

// Animate camera along curve
const moveCamera = () => {
  // console.log('movecam')
  if (percentage.run === 3.0) {
    // console.log('player')
  }
  // console.log('percentage', percentage.run)
  return percentage.run
}

// Camera
camera.lookAt(plane.position)
scene.add(camera)
//

window.addEventListener('resize', handleResize)

// eslint-disable-next-line no-unused-vars
const clock = new THREE.Clock()

/**
 * Renderer
 */

const renderer = new THREE.WebGLRenderer({
  canvas: canvas,
  antialias: true,
  alpha: true
})

renderer.outputEncoding = THREE.sRGBEncoding
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
* raycaster
* handle mouse events
* mose and click
*/

const mouse = new THREE.Vector2()
window.addEventListener('mousemove', (event) => {
  mouse.x = event.clientX / sizes.width * 2 - 1
  mouse.y = -(event.clientY / sizes.height * 2 - 1)
})

let currentIntersect = null
// TODO: to be used later
window.addEventListener('click', () => {
  if (currentIntersect) {
    const hitbox = currentIntersect.object.name
    switch (hitbox) {
      case hitboxes[0]:
        console.log('red devil')
        break
      case hitboxes[1]:
        console.log('red devil')
        break
      case hitboxes[2]:
        console.log('MCA')
        break
      case hitboxes[3]:
        console.log('Sara')
        break
      case hitboxes[4]:
        console.log('sales')
        break
    }
  }
})
const tick = () => {
  if (blenderCamera) {
    const activeCam = blenderCamera.cameras[0]
    // instead of renderer.render(scene, activeCam) use effect composer
    renderer.render(scene, activeCam)
    // use effect composer
    composer.render()
    activeCam.aspect = sizes.width / sizes.height

    activeCam.updateProjectionMatrix()

    if (hitboxesGltf) {
      const objectsToTest = hitboxesGltf.scene.children
      const raycaster = new THREE.Raycaster()
      raycaster.setFromCamera(mouse, activeCam)

      const intersects = raycaster.intersectObjects(objectsToTest, true)
      if (intersects.length) {
        if (currentIntersect === null) {
          document.body.style.cursor = 'pointer'
        }
        currentIntersect = intersects[0]
      } else {
        if (currentIntersect) {
          document.body.style.cursor = 'default'
        }
        currentIntersect = null
      }
    }

    if (mixTl) {
      mixTl.setTime(moveCamera())
    }
  }
  // composer.render()
  // Call tick again on the next frame
  window.requestAnimationFrame(tick)
}

tick()

export { phongMaterial, skyMat, log, percentage, camera, renderer, scene }
